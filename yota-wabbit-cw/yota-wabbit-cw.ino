#include <si5351.h>

/* SIMPLE MORSE ENCODER
 *  Ce programme écrit du texte manipulé en morse comme avec un clavier standard
 *  -----------------------------------------------------------------
 *  PINOUT:
 *  D3: Sortie audio (buzzer)
 *  D8: CH1 pour les points
 *  D9: CH2 pour les traits
 *  Les deux channels sont mis sur GND au point commum
 */
#include "morsecode.h"
#define TX 2
#define WPM 17
Si5351 trx;
unsigned long tdot = wpmToMillis(WPM);
unsigned long tdash = 3*tdot;
void setup()
{
 pinMode(2, OUTPUT);
  // Start serial and initialize the Si5351
  Serial.begin(57600);
  trx.init(SI5351_CRYSTAL_LOAD_8PF, 0, 0);

  // Set CLK0 to output 14 MHz
  trx.set_freq(1400000000ULL, SI5351_CLK0);

  // Enable clock fanout for the XO
  trx.set_clock_fanout(SI5351_FANOUT_XO, 1);

  // Enable clock fanout for MS
  trx.set_clock_fanout(SI5351_FANOUT_MS, 1);

  // Set CLK1 to output the XO signal
  trx.set_clock_source(SI5351_CLK1, SI5351_CLK_SRC_XTAL);
  trx.output_enable(SI5351_CLK1, 1);

  // Set CLK2 to mirror the MS0 (CLK0) output
  trx.set_clock_source(SI5351_CLK2, SI5351_CLK_SRC_MS0);
  trx.output_enable(SI5351_CLK2, 1);

  // Change CLK0 output to 10 MHz, observe how CLK2 also changes
 
 trx.set_freq(1000000000ULL, SI5351_CLK0);
  trx.update_status();
  delay(500);
}

void loop()
{
  trx.set_freq(357900000ULL, SI5351_CLK0);
  // Read the Status Register and print it every 10 seconds
  trx.update_status();
  sendMsg("VVV DE N0CALL K");
  Serial.print("  SYS_INIT: ");
  Serial.print(trx.dev_status.SYS_INIT);
  Serial.print("  LOL_A: ");
  Serial.print(trx.dev_status.LOL_A);
  Serial.print("  LOL_B: ");
  Serial.print(trx.dev_status.LOL_B);
  Serial.print("  LOS: ");
  Serial.print(trx.dev_status.LOS);
  Serial.print("  REVID: ");
  Serial.println(trx.dev_status.REVID);

  delay(10000);
}
