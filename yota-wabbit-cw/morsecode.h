/**
 * ARDUINO MORSE CODE LIBRARY
 * this library contains all the basic functions
 * to use morse code in your projects
 **/

/**
 * wpmToMillis() is a function to convert wpm
 * in milliseconds.
 * @param wpmn the tranmission speed
 * @return ptMs, the associated dot duration in milliseconds
 **/
unsigned long wpmToMillis(int wpm);

/**
 * cwToString() translates the morse code input into ASCII characters
 * @param str, the morse code character you want to convert
 **/
void cwToString(String str);

/**
 * sendChar() is the procedure sending the keycodes
 * to be interpreted as keystrokes
 * keycodes can be found here: 
 * https://www.win.tue.nl/~aeb/linux/kbd/scancodes-14.html
 * @param opcode, the hex number that matches an ASCII character
 **/
//  Alt Gr azerty                   €                                                                    ~  #  {  [  |  `  \  ^  @    ' '  ]  }  ¤       
//   Shift azerty       Q  B  C  D  E  F  G  H  I  J  K  L  ?  N  O  P  A  R  S  T  U  V  Z  X  Y  W  1  2  3  4  5  6  7  8  9  0    ' '  °  +  ¨  £  µ  No fr  M  %  NONE  .  /  §    >
//         azerty       q  b  c  d  e  f  g  h  i  j  k  l  ,  n  o  p  a  r  s  t  u  v  z  x  y  w  &  é  "  '  (  -  è  _  ç  à    ' '  )  =  ^  $  *  No fr  m  ù   ²    ;  :  !    <
//         qwerty       a  b  c  d  e  f  g  h  i  j  k  l  m  n  o  p  q  r  s  t  u  v  w  x  y  z  1  2  3  4  5  6  7  8  9  0    ' '  -  =  [  ]  \  No US  ;  '   `    ,  .  /   No US      
//       scancode       4, 5, 6, 7, 8, 9, 10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,  44, 45,46,47,48,49,  50,  51,52, 53,  54,55,56,  100}; 
void sendChar(uint8_t opcode);

/**
 * dot() sends a dot to the buzzer and the PWM pins
 * with the matching wpm speed
 **/
void dot();

/**
 * dash() sends a dot to the buzzer and the PWM pins
 * with the matching wpm speed
 **/
void dash();

/**
 * paddle() sends dots or dashes whzen using a CW
 * double contact paddle
 * @param t, the specific duration
 **/
void paddle(unsigned long t);


/**
 * sendMsg sends automatic text messages to any digital output
 * @param s, the input string
 */
void sendMsg(String s);
